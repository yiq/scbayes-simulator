#include <iostream>
#include <string>
#include <vector>
#include <cstdlib>
#include <time.h>
#include <chrono>
#include <cmath>

#include <getopt.h>

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "common.h"
#include "subclone.h"
#include "sc_simulate.h"

struct Clone *create_fixed_subclone() {
    // manual linear two node + root
    Clone *root = new Clone();
    Clone *cloneA = new Clone();
    Clone *cloneB = new Clone();

    root->children.push_back(cloneA);
    root->children.push_back(cloneB);

    assignID(root);
    //assignFraction(root);
    
    root->fraction=20;
    cloneA->fraction=30;
    cloneB->fraction=50;
    
    //assignNumvars(root, 500, 500);
    root->numvars=0;
    cloneA->numvars=500;
    cloneB->numvars=500;

    return root;
}

struct Clone *treeFromEncoding(unsigned int code, unsigned int n) {
    Clone *root = new Clone();
    root->id = 0;

    auto parent_table = new unsigned int[n];
    auto id_table = new Clone*[n+1];
    id_table[0] = root;

    for(unsigned int i=0; i<n; i++) {
        parent_table[n-i-1] = code % n;
        code = code / n;
    }

    auto& logline = logger(LOGLV_INFO)<<"DECODED: ";
    for(unsigned int i=0; i<n; i++) {
        logline<<parent_table[i];
    }
    logline<<std::endl;

    for(unsigned int i=0; i<n; i++) {
        auto newClone = new Clone();
        newClone->id = i+1;
        id_table[i+1] = newClone;
        auto *parent = id_table[parent_table[i]];
        parent->children.push_back(newClone);
        logger(LOGLV_INFO)<<"Clone "<<parent->id<<" adds a new child "<<newClone->id<<std::endl;
    }

    delete [] id_table;
    delete [] parent_table;

    return root;
}

int main(int argc, char** argv) {
    logger(LOGLV_INFO)<<__FUNCTION__<<"() starts"<<std::endl;

    auto rseed = static_cast<unsigned long>(std::chrono::high_resolution_clock::now().time_since_epoch().count());

    srand(rseed);
    logger(LOGLV_INFO)<<"random seed = "<<rseed<<std::endl;

    struct Arguments args { 
        .varCoverRate=5,
        .scDepthGeomP=0.318,
        .fpRate = 5,
        .tpRate = 0,    // use default value estimated from data
        .scErrRate = 0,
        .flatPrior = false,
        .customLikelihood = false,
        .nClusters = 1
    };

    unsigned int cloneTree = 0;
    unsigned int nClusters = 0;

    int c;
    while((c = getopt(argc, argv, "c:d:e:f:pqs:S:t:")) != -1) {
        switch(c) {
            case 'c':
                args.varCoverRate = atoi(optarg);
                if(args.varCoverRate < 0 || args.varCoverRate > 100) {
                    logger(LOGLV_ERR)<<"invalid coverage rate "<<args.varCoverRate<<std::endl;
                    exit(EXIT_FAILURE);
                }
                break;
            case 'd':
                args.scDepthGeomP = atof(optarg);
                break;
            case 'e':
                args.scErrRate = atoi(optarg);
                break;
            case 'f':
                args.fpRate = atoi(optarg);
                break;
            case 'p':
                args.flatPrior = true;
                break;
            case 'q':
                args.customLikelihood = true;
                break;
            case 's':
                cloneTree = atoi(optarg);
                break;
            case 'S':
                nClusters = atoi(optarg);
                break;
            case 't':
                args.tpRate = atoi(optarg);
                break;
            default:
                exit(EXIT_FAILURE);
        }
    }

    logger(LOGLV_INFO)<<"variant coverage rate = "<<args.varCoverRate<<"%"<<std::endl;
    logger(LOGLV_INFO)<<"depth geometric P = "<<args.scDepthGeomP<<std::endl;
    logger(LOGLV_INFO)<<"subclone error rate = "<<args.scErrRate<<std::endl;
    logger(LOGLV_INFO)<<"variant false positive rate = "<<args.fpRate<<std::endl;
    logger(LOGLV_INFO)<<"variant true positive rate = "<<args.tpRate<<std::endl;
    logger(LOGLV_INFO)<<"clone tree = "<<cloneTree<<std::endl;
    logger(LOGLV_INFO)<<"num of clusters = "<<nClusters<<std::endl;

    args.nClusters = nClusters;

    if(nClusters == 0) {
        std::cerr<<"Unable to reconstruct subclone structure with 0 clusters"<<std::endl;
        exit(1);
    }

    if(cloneTree >= tgamma(nClusters+1)) {
        std::cerr<<"Specified clone tree encoding exceeds maximum number of trees with "<<nClusters<<" clusters"<<std::endl;
        exit(1);
    }

    struct ParsedArguments parsedArgs(args);

    auto root = treeFromEncoding(cloneTree, nClusters);
    assignFraction(root);
    assignNumvars(root, 500, 2000);

    //auto root = create_fixed_subclone();

    emit_simulation_results(root, args, parsedArgs);

    delete root;

    return 0;
}
