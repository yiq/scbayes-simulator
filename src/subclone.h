#ifndef SUBCLONE_H
#define SUBCLONE_H

#include <functional>
#include "common.h"

struct Clone {
    int id;
    int fraction;
    int numvars;
    std::vector<struct Clone *> children;

    Clone() : id(0), fraction(0), numvars(0) {}
    ~Clone() {
        logger(LOGLV_INFO)<<"node "<<id<<" destructed"<<std::endl;
        for(auto *n : children)
            delete n;
    }
};

void depthFirstTraverse(struct Clone *root,
        std::function<void *(struct Clone *, void * param, std::vector<void *>)> visitor,
        void * visitorParam = nullptr,
        std::vector<void *> stateStack = std::vector<void *>());


#endif
