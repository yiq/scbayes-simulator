#ifndef SC_SIMULATE_H
#define SC_SIMULATE_H

#include "common.h"
#include "subclone.h"

struct Variant {
    int id;
    int cloneID;
    int dnaSeqDepth;
    int dnaVarCount;
};

struct Cell {
    int id;
    int cloneID;
};

int assignID(struct Clone *root, int baseID = 0);
int assignFraction(struct Clone *root, int remaining = 100);
void assignNumvars(struct Clone *root, int lowerNumvars, int upperNumvars);

template<typename OST>
void emitYaml(OST&& ost, struct Clone *root, const struct Arguments& args);

template<typename OST>
void emitClusterVCF(OST&& ost, struct Clone *root);

void emitVCFs(struct Clone *root);

template<typename OST>
void emitCellGenotypeHeader(OST&& ost, struct Clone *root);

template<typename OST>
void emitCellGenotype(OST&& ost, bool hasVariant, const struct ParsedArguments& parsedArgs);

void sc_simulate_from_clone(struct Clone *root, const struct Arguments& args, const struct ParsedArguments& parsedArgs);
void emit_simulation_results(struct Clone *root, const struct Arguments& args, const struct ParsedArguments& parsedArgs);

#endif
