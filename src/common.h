#ifndef COMMON_H
#define COMMON_H

#include <functional>
#include <random>

#include "contrib/log/log.h"

#ifndef LOGLV
#define LOGLV LOGLV_WARN
#endif

static auto logger = LOGGER(LOGLV);

struct Arguments {
    int varCoverRate;      // likelihood of a variant site having scRNAseq coverage
    float scDepthGeomP;    // P of the estimated geometric distribution for sc coverage at a covered site
    int fpRate;            // False positive rate ( likelihood a non-present variant is sequenced )
    int tpRate;            // True positive rate (if not 0, then use a flat AF (1) simulation to simulate tp)
    int scErrRate;         // Subclone structure error rate ( likelihood a variant is misplaced )
    bool flatPrior;        // Whether assignment config use bulk clonal fraction or flat prior
    bool customLikelihood;      // Whether assignment config use custom priors according to simulation
    unsigned int nClusters;// Number of clusters / subclones in the tree
};

struct ParsedArguments {

    struct Arguments args;

    std::function<bool()>   isCovered;
    std::function<int()>    simulateDepth;
    std::function<int(int)> simulateAO_tp;
    std::function<int(int)> simulateAO_fp;

    std::default_random_engine generator;
    std::uniform_int_distribution<int> duniform;
    std::geometric_distribution<int> dgeom;
    std::normal_distribution<float> dnorm;

    ParsedArguments(const struct Arguments& args) : args(args), duniform(0, 99), dgeom(args.scDepthGeomP), dnorm(0.5, 0.2) {
        isCovered = [this, args]() -> bool {
            return duniform(generator) < args.varCoverRate;
        };

        simulateDepth = [this]() -> int {
            return dgeom(generator) + 1;
        };

        simulateAO_tp = [this](int DP) -> int {
            int p_af0 = 30; // 30% of af = 0 even if the cell has the variant
            int p_af1 = 61; // 60% of af = 1 (all reads are alt reads)

            int dice = rand() % 100;
            if(dice < p_af0) {
                return 0;
            } else if(dice < (p_af0 + p_af1)) {
                return DP;
            } else {
                float af = dnorm(generator);
                if(af < 0) af = 0;
                if(af > 1) af = 1;
                return (int)(DP * af);
            }
        };

        if(args.tpRate != 0) {
            simulateAO_tp = [this](int DP) -> int {
                int dice = rand() % 100;
                if(dice < this->args.tpRate) return DP;
                else return 0;
            };
        }

        simulateAO_fp = [this, &args](int DP) -> int {
            int p_af0 = 100-args.fpRate;
            int dice = rand() % 100;
            //float af = dice < p_af0 ? 0 : 1;
            return dice < p_af0 ? 0 : rand() % DP + 1;
        };
    }
};

#endif
