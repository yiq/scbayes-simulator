#include <fstream>

#include "sc_simulate.h"

int assignID(struct Clone *root, int baseID) {
    root->id = baseID;
    auto maxID = baseID + 1;
    
    for(auto* n : root->children) {
        maxID = assignID(n, maxID);
    }

    return maxID;
}

int assignFraction(struct Clone *root, int remaining) {
    int used = 0;
    for(auto *n : root->children) {
        auto assigned = assignFraction(n, remaining);
        used += assigned;
        remaining -= assigned;
    }

    // the very root of the tree soaks up all remaining fraction
    if(root->id == 0) root->fraction = remaining;
    else root->fraction = rand() % remaining;

    return root->fraction + used;
}

void assignNumvars(struct Clone *root, int lowerNumvars, int upperNumvars) {
    if(root->id != 0)
        root->numvars = rand() % (upperNumvars - lowerNumvars) + lowerNumvars;
    logger(LOGLV_INFO)<<"subclone "<<root->id<<" assigned "<<root->numvars<<" vars"<<std::endl;
    for(auto *n : root->children) assignNumvars(n, lowerNumvars, upperNumvars);
}

template<typename OST>
void emitYaml(OST&& ost, struct Clone *root, const struct Arguments& args) {

    // emit data likelihood
    if(args.customLikelihood) {
        ost<<"data_likelihoods:"<<std::endl;
        ost<<"    false_positive: 0."<<(args.fpRate < 10 ? "0" : "")<<args.fpRate<<std::endl;
        ost<<"    true_positive: 0."<<(args.tpRate == 0 ? "7" : std::to_string(args.tpRate))<<std::endl;
    }

    // emit clusters
    ost<<"clusters:"<<std::endl;

    auto emitClusterByClone = [&ost](struct Clone *node, void * param, std::vector<void *> state) -> void* {
        if(node->id == 0) return nullptr;
        ost<<"    - name: C"<<node->id<<std::endl;
        ost<<"      vcf: C"<<node->id<<".vcf"<<std::endl;
        return nullptr;
    };

    depthFirstTraverse(root, emitClusterByClone);

    ost<<std::endl;
    ost<<"subclones:"<<std::endl;

    auto emitSubcloneByClone = [&ost, &args](struct Clone *node, void * param, std::vector<void *> state) -> void* {
        ost<<"    - name: "<<(node->id == 0 ?
                "normal" : 
                (std::string("SC") + std::to_string(node->id)))
            <<std::endl;
        int fraction = args.flatPrior ? (1.0 / (args.nClusters + 1) * 100) : node->fraction;

        ost<<"      fraction: 0."<< (fraction < 10 ? "0" : "")<<fraction<<std::endl;
        ost<<"      clusters: [";
        for(auto *s : state) {
            auto *n = reinterpret_cast<struct Clone *>(s);
            if(n != nullptr) ost<<"C"<<n->id<<",";
        }
        if(node->id != 0) ost<<"C"<<node->id;
        ost<<"]"<<std::endl;
        ost<<"      defining_cluster: \"";
        if(node->id != 0) ost<<"C"<<node->id;
        ost<<"\""<<std::endl;
        return node->id != 0 ? node : nullptr;
    };

    depthFirstTraverse(root, emitSubcloneByClone);
}

template<typename OST>
void emitClusterVCF(OST&& ost, struct Clone *root) {
    ost<<"##fileformat=VCFv4.1"<<std::endl;
    ost<<"##contig=<ID=chr"<<root->id<<",length="<<root->numvars<<">"<<std::endl;
    ost<<"#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO"<<std::endl;
    for(auto i=0; i<root->numvars; i++) {
        ost<<"chr"<<root->id<<"\t"<<i+1<<"\t.\t";
        ost<<"R\tA\t255.00\t.\t."<<std::endl;
    }
}

void emitVCFs(struct Clone *root) {
    std::string fn = "C" + std::to_string(root->id)+".vcf";
    if(root->id != 0) emitClusterVCF(std::ofstream(fn), root);
    for(auto* n : root->children) emitVCFs(n);
}

template<typename OST>
void emitCellGenotypeHeader(OST&& ost, struct Clone *root) {
    for(int i=0; i<root->fraction*10; i++) {
        ost<<"\tCB_"<<root->id<<"_"<<i;
    }
    for(struct Clone *n : root->children)
        emitCellGenotypeHeader(ost, n);
}

template<typename OST>
void emitCellGenotype(OST&& ost, bool hasVariant, const struct ParsedArguments& parsedArgs) {
    // sample if cell has coverage
    if(parsedArgs.isCovered()) {
        // sample coverage
        int coverage = parsedArgs.simulateDepth();
        int alt = hasVariant ? parsedArgs.simulateAO_tp(coverage) : parsedArgs.simulateAO_fp(coverage);
        ost<<coverage<<":"<<coverage - alt<<":"<<alt;
    }
    else {
        ost<<"0:0:0";
    }
}


void sc_simulate_from_clone(struct Clone *root, const struct Arguments& args, const struct ParsedArguments& parsedArgs) {
    assignID(root);
    assignFraction(root);
    assignNumvars(root, 500, 2000);

    emit_simulation_results(root, args, parsedArgs);
}

void emit_simulation_results(struct Clone *root, const struct Arguments& args, const struct ParsedArguments& parsedArgs) {
    emitYaml(std::ofstream("simulate.assign.yaml"), root, args);
    emitVCFs(root);

    std::ofstream gtOfs("genotypes.tsv");
    std::ofstream matrixOfs("varmatrix.tsv");
    
    auto simulateCellBarcodes = [&gtOfs](struct Clone *node, void * param, std::vector<void *>state) -> void* {
        for(int i=0; i<node->fraction; i++) {
            gtOfs<<"\t"<<"CB_";
            for(auto *s : state) gtOfs<<reinterpret_cast<struct Clone *>(s)->id;
            gtOfs<<node->id<<"_"<<i;
        }
        return node;
    };
    gtOfs<<"variants";
    depthFirstTraverse(root, simulateCellBarcodes);
    gtOfs<<std::endl;

    auto emitVarMatrixHeader = [&matrixOfs](struct Clone *node, void *param, std::vector<void *>state) -> void* {
        matrixOfs<<"\t"<<(node->id == 0 ?
                "normal" : 
                (std::string("SC") + std::to_string(node->id)));
        return node;
    };
    matrixOfs<<"variants";
    depthFirstTraverse(root, emitVarMatrixHeader);
    matrixOfs<<std::endl;

    struct varIterParam_t {
        struct Clone * node;
        bool flipHasVar;
    } varIterParam;

    // simulate variants and genotypes of each cell as it traverses the tree
    auto variantIterator = [&root, &gtOfs, &matrixOfs, &parsedArgs, &varIterParam](struct Clone *node, void * param, std::vector<void *> state) -> void * {
        auto &logStm = logger(LOGLV_INFO)<<"variantIterator at subclone ";
        for(auto *s : state) logStm<<reinterpret_cast<struct Clone *>(s)->id<<"->";
        logStm<<node->id<<std::endl;

        /**** PART 1: single cell genotype emittion ****/
        // simualte cell genotype as it traverses the tree
        auto simulateCellGenotypes = [&gtOfs, &parsedArgs](struct Clone *node, void * param, std::vector<void *>state) -> void* {

            // facts: 
            //   1. cells simulated in this run come from subclone *node*
            //   2. therefore, should contain variants in this subclone
            //   3. as well as variants of its ancestral clones

            auto * iterParam = static_cast<struct varIterParam_t *>(param);
            //struct Clone* varClone = reinterpret_cast<struct Clone *>(param);
            bool cellHasVar = false;
            if(iterParam->node->id == node->id) cellHasVar = true;
            for(auto *s : state) if(reinterpret_cast<struct Clone *>(s)->id == iterParam->node->id) cellHasVar = true;

            if(iterParam->flipHasVar) cellHasVar = not cellHasVar;

            for(int i=0; i<node->fraction; i++) {
                gtOfs<<"\t";
                emitCellGenotype(gtOfs, cellHasVar, parsedArgs);
            }
            return node;
        };

        /**** PART 2: variant matrix emittion ****/
        auto emitVarMatrix = [&matrixOfs](struct Clone *node, void *param, std::vector<void *>state) -> void * {
            auto * iterParam = static_cast<struct varIterParam_t *>(param);
            //struct Clone * varClone = reinterpret_cast<struct Clone *>(param);
            bool cloneHasVar = false;
            if(iterParam->node->id == node->id) cloneHasVar = true;
            for(auto *s : state) if(reinterpret_cast<struct Clone *>(s)->id == iterParam->node->id) cloneHasVar = true;
            matrixOfs<<"\t"<<(cloneHasVar ? 1 : 0);
            return node;
        };

        // emunerate variant
        
        int goodVars = node->numvars * (float)(100 - parsedArgs.args.scErrRate) / 100.0;

        for(int i=0; i<node->numvars; i++) {
            gtOfs<<"chr"<<node->id<<":"<<i+1<<":R:A";
            matrixOfs<<"chr"<<node->id<<":"<<i+1<<":R:A";
            varIterParam.node = node;
            varIterParam.flipHasVar = i > goodVars;
            // simulate cell genotypes for this variant
            depthFirstTraverse(root, simulateCellGenotypes, &varIterParam);
            depthFirstTraverse(root, emitVarMatrix, &varIterParam);
            gtOfs<<std::endl;
            matrixOfs<<std::endl;
        }

        return node;
    };

    depthFirstTraverse(root, variantIterator);


}
