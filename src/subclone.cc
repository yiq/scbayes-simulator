#include <fstream>
#include "subclone.h"

void depthFirstTraverse(struct Clone *root,
        std::function<void *(struct Clone *, void * param, std::vector<void *>)> visitor,
        void * visitorParam,
        std::vector<void *> stateStack) {
    void * state = visitor(root, visitorParam, stateStack);
    stateStack.push_back(state);
    for(auto *n : root->children) {
        depthFirstTraverse(n, visitor, visitorParam, stateStack);
    }
    stateStack.pop_back();
}


