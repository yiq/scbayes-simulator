#!/bin/bash

module load singularity scbayes/0.1.5 htslib/1.9 gcc/10.2.0

PRIOR="Informed Flat"
REPEATS=100

simulate() {
    if [ "x$1" == "xFlat" ]; then
        ../src/scSimulate -f 20 -e 30 -p -q -S 2 -s 1
    else
        ../src/scSimulate -f 20 -e 30 -q -S 2 -s 1
    fi
}

job_id() {
    echo "FPR_$1"
}

sim_setup() {
    jid=$1
    echo "setup with jid $jid"
    rm scbayes-$jid.tsv
    rm cardelino-$jid.tsv
    echo $jid > scbayes-$jid.tsv
    echo $jid > cardelino-$jid.tsv
    mkdir $jid
}

sim_task() {
    jid=$(job_id $1)
    sim_setup $jid
    cd $jid
    for i in `seq $REPEATS`
    do
        simulate $1

        ../genotype2ad.sh
        scAssign simulate.assign.yaml genotypes.tsv > scbayes.results.tsv
        singularity exec $HOME/cardelino.sif Rscript ../sim2cardelino.R

        cat scbayes.results.tsv | ../score.sh >> ../scbayes-$jid.tsv
        cat cardelino.results.tsv | ../score.sh >> ../cardelino-$jid.tsv
    done
    cd ..
    rm -rf $jid
}

PASTE_LIST_SC=""
PASTE_LIST_CD=""

for pr in $PRIOR
do
    sim_task $pr &
    jid=$(job_id $pr)
    PASTE_LIST_SC="$PASTE_LIST_SC scbayes-$jid.tsv"
    PASTE_LIST_CD="$PASTE_LIST_CD cardelino-$jid.tsv"
done

wait

paste $PASTE_LIST_SC > scbayes-flatpr.tsv
paste $PASTE_LIST_CD > cardelino-flatpr.tsv

rm $PASTE_LIST_SC
rm $PASTE_LIST_CD
