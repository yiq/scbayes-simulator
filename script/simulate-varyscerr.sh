#!/bin/bash

module load singularity scbayes/0.1.5 htslib/1.9 gcc/10.2.0

FPRATE=5
SCERRS="5 10 20 30 40"
REPEATS=100

sim_task() {
    scerr=$1
    TDIR=sim-scerr$scerr
    rm -f scbayes-scerr$scerr.tsv
    rm -f cardelino-scerr$scerr.tsv
    echo $scerr> scbayes-scerr$scerr.tsv
    echo $scerr> cardelino-scerr$scerr.tsv
    mkdir $TDIR
    cd $TDIR
    for i in `seq $REPEATS`
    do
        ../src/scSimulate -f $FPRATE -e $scerr -q -S 2 -s 0
        ../genotype2ad.sh
        scAssign simulate.assign.yaml genotypes.tsv > scbayes.results.tsv
        singularity exec $HOME/cardelino.sif Rscript ../sim2cardelino.R
        cat scbayes.results.tsv | ../score.sh >> ../scbayes-scerr$scerr.tsv
        cat cardelino.results.tsv | ../score.sh >> ../cardelino-scerr$scerr.tsv
    done
    cd ..
    rm -rf $TDIR
}

PASTE_LIST_SC=""
PASTE_LIST_CD=""

for scerr in $SCERRS
do
    sim_task $scerr &
    PASTE_LIST="$PASTE_LIST scbayes-scerr$scerr.tsv"
    PASTE_LIST_SC="$PASTE_LIST_SC scbayes-scerr$scerr.tsv"
    PASTE_LIST_CD="$PASTE_LIST_CD cardelino-scerr$scerr.tsv"
done

wait

paste $PASTE_LIST_SC > scbayes-varyscerr.tsv
paste $PASTE_LIST_CD > cardelino-varyscerr.tsv
rm $PASTE_LIST_SC
rm $PASTE_LIST_CD
