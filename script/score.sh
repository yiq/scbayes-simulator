#!/bin/bash

grep -v ^Bar |
    cut -f1,2 |
    sed 's/^CB_[0-9]*\([0-9]\)_[0-9]*/\1/' |
    sed 's/SC//' |
    sed 's/^0/normal/' | 
    sed -n 's/\([^\s]\+\)\t\1/\0/p' |
    wc -l
