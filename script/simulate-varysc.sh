#!/bin/bash

module load singularity scbayes/0.1.5 htslib/1.9 gcc/10.2.0

NCLONES=$1

if [ "x$NCLONES" == "x" ]
then
    echo "NCLONES not specified, default to 3"
    NCLONES=3
fi

REPEATS=100
FP_RATE=5

sim_task() {
    subclone=$1
    TDIR=sim-sc${NCLONES}_$subclone
    rm -f scbayes-sc${NCLONES}_${subclone}.tsv
    rm -f cardelino-sc${NCLONES}_${subclone}.tsv
    echo sc${NCLONES}_$subclone > scbayes-sc${NCLONES}_$subclone.tsv
    echo sc${NCLONES}_$subclone > cardelino-sc${NCLONES}_$subclone.tsv
    mkdir $TDIR
    cd $TDIR
    for i in `seq $REPEATS`
    do
        ../src/scSimulate -f $FP_RATE -q -S $NCLONES -s $subclone
        ../genotype2ad.sh
        scAssign simulate.assign.yaml genotypes.tsv > scbayes.results.tsv
        singularity exec $HOME/cardelino.sif Rscript ../sim2cardelino.R
        cat scbayes.results.tsv | ../score.sh >> ../scbayes-sc${NCLONES}_$subclone.tsv
        cat cardelino.results.tsv | ../score.sh >> ../cardelino-sc${NCLONES}_$subclone.tsv
    done
    cd ..
    rm -rf $TDIR
}

PASTE_LIST_SC=""
PASTE_LIST_CD=""

N_SUBCLONES=$(seq -s "*" 1 $NCLONES | bc)

for s in $(seq $N_SUBCLONES)
do
    subclone=$((s - 1))
    sim_task $subclone &
    PASTE_LIST_SC="$PASTE_LIST_SC scbayes-sc${NCLONES}_$subclone.tsv"
    PASTE_LIST_CD="$PASTE_LIST_CD cardelino-sc${NCLONES}_$subclone.tsv"
done

wait

paste $PASTE_LIST_SC > scbayes-varysc_${NCLONES}.tsv
paste $PASTE_LIST_CD > cardelino-varysc_${NCLONES}.tsv
rm $PASTE_LIST_SC
rm $PASTE_LIST_CD
