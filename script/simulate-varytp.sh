#!/bin/bash

module load singularity scbayes/0.1.5 htslib/1.9 gcc/10.2.0

TPRATES="95 90 80 70 60 50"
REPEATS=100

simulate() {
    ../src/scSimulate -f 5 -t $1 -q -S 3 -s 3
}

job_id() {
    echo "tp$1"
}

sim_setup() {
    jid=$1
    echo "setup with jid $jid"
    rm scbayes-$jid.tsv
    rm cardelino-$jid.tsv
    echo $jid > scbayes-$jid.tsv
    echo $jid > cardelino-$jid.tsv
    mkdir $jid
}

sim_task() {
    jid=$(job_id $1)
    sim_setup $jid
    cd $jid
    for i in `seq $REPEATS`
    do
        simulate $1

        ../genotype2ad.sh
        scAssign simulate.assign.yaml genotypes.tsv > scbayes.results.tsv
        singularity exec $HOME/cardelino.sif Rscript ../sim2cardelino.R

        cat scbayes.results.tsv | ../score.sh >> ../scbayes-$jid.tsv
        cat cardelino.results.tsv | ../score.sh >> ../cardelino-$jid.tsv
    done
    cd ..
    rm -rf $jid
}

PASTE_LIST_SC=""
PASTE_LIST_CD=""

for tprate in $TPRATES
do
    sim_task $tprate &
    jid=$(job_id $tprate)
    PASTE_LIST_SC="$PASTE_LIST_SC scbayes-$jid.tsv"
    PASTE_LIST_CD="$PASTE_LIST_CD cardelino-$jid.tsv"
done

wait

paste $PASTE_LIST_SC > scbayes-varytp.tsv
paste $PASTE_LIST_CD > cardelino-varytp.tsv

rm $PASTE_LIST_SC
rm $PASTE_LIST_CD
