#!/bin/bash

cat genotypes.tsv | sed -e 's/\([0-9]\+\):\([0-9]\+\):\([0-9]\+\)/\1/g' > DP.txt
cat genotypes.tsv | sed -e 's/\([0-9]\+\):\([0-9]\+\):\([0-9]\+\)/\3/g' > AD.txt
